<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('login', ['uses' => 'AuthController@login']);

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('/ubah-pwd', ['uses' => 'AuthController@ubahPwd']);
    $router->post('/logout', ['uses' => 'AuthController@logOut']);
    
    $router->group(['prefix' => 'anggota'], function () use ($router) { 
        // $router->get('/', ['middleware' => 'can:data-aktivitas-index', 'uses' => 'DataAktivitasController@index']);
        $router->get('/', ['uses' => 'AnggotaController@index']);
        $router->get('/{id}', ['uses' => 'AnggotaController@show']);
        $router->post('/', ['uses' => 'AnggotaController@store']);
        $router->put('/{id}', ['uses' => 'AnggotaController@update']);
        $router->delete('/{id}', ['uses' => 'AnggotaController@destroy']);
        
        $router->get('/{id}/simpanan-pokok', ['uses' => 'AnggotaController@simpananPokok']);
        $router->get('/{id}/simpanan-wajib', ['uses' => 'AnggotaController@simpananWajib']);
        $router->get('/{id}/tabungan', ['uses' => 'AnggotaController@tabungan']);
        $router->post('/{id}/tabungan', ['uses' => 'AnggotaController@tabunganBaru']);
        $router->get('/{id}/pinjaman', ['uses' => 'AnggotaController@pinjaman']);
        $router->post('/{id}/pinjaman', ['uses' => 'AnggotaController@pinjamanPencairan']);
    });
    
    $router->group(['prefix' => 'simpanan-pokok'], function () use ($router) {
        $router->get('/{id}/riwayat', ['uses' => 'SimpananPokokController@riwayat']);
        $router->post('/{id}/setor', ['uses' => 'SimpananPokokController@setor']);
        $router->post('/{id}/tarik', ['uses' => 'SimpananPokokController@tarik']);
    });
    
    $router->group(['prefix' => 'simpanan-wajib'], function () use ($router) {
        $router->get('/{id}/riwayat', ['uses' => 'SimpananWajibController@riwayat']);
        $router->post('/{id}/setor', ['uses' => 'SimpananWajibController@setor']);
        $router->post('/{id}/tarik', ['uses' => 'SimpananWajibController@tarik']);
    });
    
    $router->group(['prefix' => 'tabungan'], function () use ($router) {
        $router->get('/{id}/riwayat', ['uses' => 'TabunganController@riwayat']);
        $router->post('/{id}/setor', ['uses' => 'TabunganController@setor']);
        $router->post('/{id}/tarik', ['uses' => 'TabunganController@tarik']);
    });

    $router->group(['prefix' => 'pinjaman'], function () use ($router) {
        $router->get('/{id}/tagihan', ['uses' => 'PinjamanController@tagihan']);
        $router->post('/{id}/bayar', ['uses' => 'PinjamanController@bayar']);
    });
});