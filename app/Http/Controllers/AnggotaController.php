<?php

namespace App\Http\Controllers;

use App\Models\Anggota;     
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Models\Tabungan\Master as Tabungan;
use App\Models\SimpananPokok\Master as SimpananPokok;
use App\Models\SimpananWajib\Master as SimpananWajib;

class AnggotaController extends Controller
{
    public function index(Request $request)
    {
        $cari = trim($request->cari);

        $datas = Anggota::when(!empty($cari), function ($query) use ($cari) {
            return $query->where('nama', 'LIKE', '%'. $cari .'%');
        })
        ->get();

        return response()->json($datas, 200);
    }

    public function show($id)
    {
        $anggota = Anggota::findOrFail($id);

        return response()->json($anggota);
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => ['required', 'string', 'max:255'],
            'alamat' => ['required', 'string', 'max:255'],
            // 'status' => ['required', Rule::in([Anggota::STATUS_ANGGOTA, Anggota::STATUS_NONANGGOTA])],
            'nominal_pokok' => ['required', 'numeric', 'min:0'],
            'nominal_wajib' => ['required', 'numeric', 'min:0'],
        ]);

        $anggota = Anggota::create([
            'nomor' => Anggota::genNomor(), 
            'nama' => $request->nama, 
            'alamat' => $request->alamat, 
            'join_at' => date("Y-m-d H:i:s"), 
            'status' => Anggota::STATUS_ANGGOTA,
            'created_by' => Auth::id(),
        ]);

        $simpananPokok = $anggota->simpananPokok()->create([
            'nomor' => SimpananPokok::genNomor(), 
            'saldo_akhir' => 0,
        ]);
        $simpananWajib = $anggota->simpananWajib()->create([
            'nomor' => SimpananWajib::genNomor(), 
            'saldo_akhir' => 0,
        ]);

        if ($request->nominal_pokok > 0) {
            $simpananPokok->setor($request->nominal_pokok);
        }
        if ($request->nominal_wajib > 0) {
            $simpananWajib->setor($request->nominal_wajib);
        }

        return response()->json($anggota);
    }

    public function update(Request $request, $id)
    {
        $anggota = Anggota::findOrFail($id);

        $this->validate($request, [
            'nama' => ['required', 'string', 'max:255'],
            'alamat' => ['required', 'string', 'max:255'],
        ]);

        $anggota = Anggota::create([
            'nama' => $request->nama, 
            'alamat' => $request->alamat,
        ]);

        return response()->json($anggota);
    }
    
    public function destroy($id)
    {
        $anggota = Anggota::findOrFail($id);

        $anggota->delete();

        return response()->json(['msg' => "Berhasil menghapus anggota."]);
    }

#produk
    public function simpananPokok($id)
    {
        $anggota = Anggota::findOrFail($id);

        return response()->json($anggota->simpananPokok);
    }

    public function simpananWajib($id)
    {
        $anggota = Anggota::findOrFail($id);

        return response()->json($anggota->simpananWajib);
    }

    public function tabungan($id)
    {
        $anggota = Anggota::findOrFail($id);

        return response()->json($anggota->tabungan);
    }

    public function tabunganBaru(Request $request, $id)
    {
        $anggota = Anggota::findOrFail($id);

        $this->validate($request, [
            'nominal' => ['required', 'numeric', 'min:1'],
        ]);

        $tabungan = $anggota->tabungan()->create([
            'nomor' => Tabungan::genNomor(), 
            'saldo_akhir' => 0, 
            'saldo_tinggi' => 0, 
            'saldo_rendah' => 0, 
            'tanggal' => date("Y-m-d H:i:s"), 
            'created_by' => Auth::id(),
        ]);

        $tabungan->setor($request->nominal, "Setoran Awal Tabungan");

        return response()->json($tabungan);
    }

    public function pinjamanPencairan(Request $request, $id)
    {
        $anggota = Anggota::findOrFail($id);

        // $this->validate($request, [
        //     'nominal' => ['required', 'numeric', 'min:1'],
        // ]);

        
    }

    public function pinjaman($id)
    {
        $anggota = Anggota::findOrFail($id);

        $anggota->load("pinjamanAktif.pembayaran");

        return response()->json($anggota->pinjamanAktif);
    }
#
}
