<?php

namespace App\Http\Controllers;

use App\Models\Pinjaman\Master;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    public function tagihan(Request $request, $id)
    {
        $master = Master::findOrFail($id);

        return response()->json($master->tagihan());
    }

    public function bayar(Request $request, $id)
    {
        $master = Master::findOrFail($id);

        $this->validate($request, [
            'pokok' => ['required', 'numeric', 'min:0'],
            'bunga' => ['required', 'numeric', 'min:0'],
            'denda' => ['required', 'numeric', 'min:0'],
        ]);

        $pembayaran = $master->bayar($request->pokok, $request->bunga, $request->denda);

        return response()->json($pembayaran);
    }
}
