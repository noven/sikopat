<?php

namespace App\Http\Controllers;

use App\Models\SimpananWajib\Master;
use Illuminate\Http\Request;

class SimpananWajibController extends Controller
{
    public function setor(Request $request, $id)
    {
        $master = Master::findOrFail($id);

        $this->validasi($request);

        $transaction = $master->setor($request->nominal);

        return response()->json($transaction);
    }

    public function tarik(Request $request, $id)
    {
        $master = Master::findOrFail($id);

        $this->validasi($request);

        $transaction = $master->tarik($request->nominal);

        return response()->json($transaction);
    }

    public function riwayat($id)
    {
        $master = Master::findOrFail($id);

        return response()->json($master->transactions);
    }

    private function validasi(Request $request)
    {
        $this->validate($request, [
            'nominal' => ['required', 'numeric', 'min:1'],
        ]);
    }
}
