<?php

namespace App\Http\Controllers;

use App\Models\Tabungan\Master;
use Illuminate\Http\Request;

class TabunganController extends Controller
{
    public function setor(Request $request, $id)
    {
        $master = Master::findOrFail($id);

        $this->validasi($request);

        $transaction = $master->setor($request->nominal, $request->keterangan);

        return response()->json($transaction);
    }

    public function tarik(Request $request, $id)
    {
        $master = Master::findOrFail($id);

        $this->validasi($request);

        $transaction = $master->tarik($request->nominal, $request->keterangan);

        return response()->json($transaction);
    }

    public function riwayat($id)
    {
        $master = Master::findOrFail($id);

        return response()->json($master->transactions);
    }

    private function validasi(Request $request)
    {
        $this->validate($request, [
            'nominal' => ['required', 'numeric', 'min:1'],
            'keterangan' => ['nullable',  'string', 'max:255'],
        ]);
    }
}
