<?php

namespace App\Http\Controllers;

use App\Models\AccessToken;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Carbon\Carbon;

class AuthController extends Controller
{
    const SECRET = "564s6g4egegs54";

    private $user;
    
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
        if ($this->checkAplikasi($request)) {
            $this->validate($request, [
                'username' => 'required|string',
                'password' => 'required|string',
            ]);

            if ($this->attemptLogin($request)) {
                return $this->genSendTokenResponse();
            } else {
                return response()->json(['msg' => "Username atau Password Salah"], 456);
            }
        }

        return response()->json(['msg' => "Aplikasi Ditolak Dari Server"], 456);
    }

    public function checkAplikasi(Request $request)
    {
        if (is_null($request->secret)) {
            return false;
        }

        if ($request->secret != self::SECRET) {
            return false;
        }

        return true;
    }

    private function credentials(Request $request)
    {
        return $request->only('username', 'password');
    }

    public function findUser(Request $request)
    {
        $credentials = $this->credentials($request);
        $where = [];
        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'password')) {
                continue;
            }
            array_push($where, [$key, $value]);
        }

        $this->user = User::where($where)->first();
    }

    private function attemptLogin(Request $request)
    {
        $this->findUser($request);
        return ! is_null($this->user) && $this->validateCredentials($this->user, $this->credentials($request));
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {
        $plain = $credentials['password'];

        return Hash::check($plain, $user->getAuthPassword());
    }

    private function genSendTokenResponse()
    {
        $token = Str::random(32);

        $accessToken = AccessToken::create([
            'id' => hash('sha256', $token), 
            'user_id' => $this->user->id, 
            'revoked' => false, 
            'expires_at' => Carbon::now()->addMinutes(AccessToken::TOKEN_LIFETIME_KOLEKTOR),
            'device' => $_SERVER['HTTP_USER_AGENT']
        ]);

        return response()->json([
            'token' => $token,
            'username' => $this->user->username,
            'role' => $this->user->role->nama,
        ]);
    }

    public function logout(Request $request)
    {
        $token = Auth::accessToken();

        $token->update(['revoked' => true]);

        return response()->json(['msg' => "Berhasil Logout"], 200);
    }   

    public function ubahPwd(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'password_lama' => [function ($attribute, $value, $fail) use ($user) {
                if (!Hash::check($value, $user->password)) {
                    $fail('Password lama salah.');
                }
            }],
            'password' => ['required', 'string', 'max:255', 'min:8', 'confirmed'],
        ]);

        $user->update(
            ['password' => Hash::make($request->password)]
        );

        return $user;
    }
}
