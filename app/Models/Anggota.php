<?php

namespace App\Models;

use App\Helpers\Number;
use App\Models\SimpananPokok\Master as SimpananPokok;
use App\Models\SimpananWajib\Master as simpananWajib;
use App\Models\Tabungan\Master as Tabungan;
use App\Models\Pinjaman\Master as Pinjaman;
use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{   
    const STATUS_ANGGOTA = 1;
    const STATUS_NONANGGOTA = 2;

    protected $table = 'anggota';

    protected $fillable = [ 
        'nomor', 'nama', 'alamat', 'join_at', 'status', 'created_by',
    ];

    public function simpananPokok()
    {
        return $this->hasOne(SimpananPokok::class, 'anggota_id');
    }

    public function simpananWajib()
    {
        return $this->hasOne(simpananWajib::class, 'anggota_id');
    }
    
    public function tabungan()
    {
        return $this->hasMany(Tabungan::class, 'anggota_id');
    }

    public function pinjaman()
    {
        return $this->hasMany(Pinjaman::class, 'anggota_id');
    }

    public function pinjamanAktif()
    {
        return $this->hasOne(Pinjaman::class, 'anggota_id')
        ->whereNull('lunas_at');
    }

    public static function genNomor()
    {
        $prefix = '';
        $angka = '00000';

        $kode = self::orderBy('nomor', 'desc')
        ->pluck('nomor')
        ->first();
        
        return Number::autoNumber(($kode ?: $prefix.$angka), strlen($prefix), strlen($angka));
    }
}