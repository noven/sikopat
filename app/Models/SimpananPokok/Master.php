<?php

namespace App\Models\SimpananPokok;

use App\Helpers\Number;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Master extends Model
{   
    protected $table = 'simpanan_pokok_master';

    protected $fillable = [ 
        'nomor', 'anggota_id', 'saldo_akhir',
    ];

    public function transactions()
    {
        return $this->hasMany(Transactions::class, 'master_id');
    }

    public static function genNomor()
    {
        $prefix = '';
        $angka = '00000';

        $kode = self::orderBy('nomor', 'desc')
        ->pluck('nomor')
        ->first();
        
        return Number::autoNumber(($kode ?: $prefix.$angka), strlen($prefix), strlen($angka));
    }

    public function setor($nominal)
    {
        $saldoAkhir = $this->saldo_akhir + $nominal;

        $transaction = $this->transactions()->create([
            'debet' => 0, 
            'kredit' => $nominal, 
            'saldo' => $saldoAkhir,
            'tanggal' => date("Y-m-d H:i:s"), 
            'created_by' => Auth::id(),
        ]);

        $this->update([
            "saldo_akhir" => $saldoAkhir
        ]);

        return $transaction;
    }

    public function tarik($nominal)
    {
        $saldoAkhir = $this->saldo_akhir - $nominal;

        $transaction = $this->transactions()->create([
            'debet' => $nominal, 
            'kredit' => 0, 
            'saldo' => $saldoAkhir, 
            'tanggal' => date("Y-m-d H:i:s"), 
            'created_by' => Auth::id(),
        ]);

        $this->update([
            "saldo_akhir" => $saldoAkhir
        ]);

        return $transaction;
    }
}