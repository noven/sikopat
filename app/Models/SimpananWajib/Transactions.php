<?php

namespace App\Models\SimpananWajib;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{   
    protected $table = 'simpanan_wajib_trx';

    protected $fillable = [ 
        'master_id', 'debet', 'kredit', 'saldo', 'tanggal', 'created_by',
    ];

    public function master()
    {
        return $this->belongsTo(Master::class, 'master_id');
    }
}