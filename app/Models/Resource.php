<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'nama', 'param',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}