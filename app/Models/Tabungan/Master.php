<?php

namespace App\Models\Tabungan;

use App\Helpers\Number;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Master extends Model
{   
    const JENIS_SETORAN = 1;
    const JENIS_PENARIKAN = 2;
    const JENIS_BUNGA = 3;

    protected $table = 'tabungan_master';

    protected $fillable = [ 
        'nomor', 'anggota_id', 'saldo_akhir', 'saldo_tinggi', 'saldo_rendah', 'tanggal', 'created_by',
    ];

    public function transactions()
    {
        return $this->hasMany(Transactions::class, 'master_id');
    }

    public static function genNomor()
    {
        $prefix = '';
        $angka = '00000';

        $kode = self::orderBy('nomor', 'desc')
        ->pluck('nomor')
        ->first();
        
        return Number::autoNumber(($kode ?: $prefix.$angka), strlen($prefix), strlen($angka));
    }

    public function genSaldo($saldoAkhir)
    {
        $this->saldo_akhir = $saldoAkhir;
        $this->saldo_tinggi = $saldoAkhir > $this->saldo_tinggi ? $saldoAkhir : $this->saldo_tinggi;
        $this->saldo_rendah = $saldoAkhir < $this->saldo_rendah ? $saldoAkhir : $this->saldo_rendah;

        $this->save();
    }

    public function setor($nominal, $keterangan = "Setoran Tabungan")
    {
        $saldoAkhir = $this->saldo_akhir + $nominal;

        $transaction = $this->transactions()->create([
            'debet' => 0, 
            'kredit' => $nominal, 
            'saldo' => $saldoAkhir, 
            'keterangan' => $keterangan, 
            'jenis' => self::JENIS_SETORAN, 
            'tanggal' => date("Y-m-d H:i:s"), 
            'created_by' => Auth::id(),
        ]);

        $this->genSaldo($saldoAkhir);

        return $transaction;
    }

    public function tarik($nominal, $keterangan = "Penarikan Tabungan")
    {
        $saldoAkhir = $this->saldo_akhir - $nominal;

        $transaction = $this->transactions()->create([
            'debet' => $nominal, 
            'kredit' => 0, 
            'saldo' => $saldoAkhir, 
            'keterangan' => $keterangan, 
            'jenis' => self::JENIS_PENARIKAN, 
            'tanggal' => date("Y-m-d H:i:s"), 
            'created_by' => Auth::id(),
        ]);

        $this->genSaldo($saldoAkhir);

        return $transaction;
    }
}