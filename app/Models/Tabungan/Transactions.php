<?php

namespace App\Models\Tabungan;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{   
    protected $table = 'tabungan_trx';

    protected $fillable = [ 
        'master_id', 'debet', 'kredit', 'saldo', 'keterangan', 'jenis', 'tanggal', 'created_by',
    ];

    public function master()
    {
        return $this->belongsTo(Master::class, 'master_id');
    }
}