<?php

namespace App\Models\Pinjaman;

use App\Helpers\Number;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Master extends Model
{   
    const BUNGA_MENETAP = 1;
    const BUNGA_MUNURUN = 2;

    protected $table = 'pinjaman_master';

    protected $fillable = [ 
        'nomor', 'jaminan', 'anggota_id', 'nominal', 'potongan', 'uang_diterima', 'bunga', 'jenis_bunga', 'angsuran', 'tanggal_cair', 'tanggal_jatuh_tempo', 'jangka_waktu', 'tanggal_pembayaran_selanjutnya', 'sisa', 'lunas_at', 'created_by',
    ];

    public function anggota()
    {
        return $this->belongsTo(Anggota::class, 'anggota_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function pembayaran()
    {
        return $this->hasMany(Pembayaran::class, 'master_id');
    }

    public function potongan()
    {
        return $this->hasMany(Potongan::class, 'master_id');
    }

    public function simulasi()
    {
        return $this->hasMany(Simulasi::class, 'master_id');
    }

    public static function genNomor()
    {
        $prefix = '';
        $angka = '00000';

        $kode = self::orderBy('nomor', 'desc')
        ->pluck('nomor')
        ->first();
        
        return Number::autoNumber(($kode ?: $prefix.$angka), strlen($prefix), strlen($angka));
    }

    public function hitungBunga()
    {
        switch ($this->jenis_bunga) {
            case self::BUNGA_MENETAP:
                return $this->nominal * $this->bunga / 100;

            case self::BUNGA_MUNURUN:
                return $this->sisa * $this->bunga / 100;
            
            default:
                return 0;
        }
    }

    public function pencairan()
    {
        # code...
    }

    public function tagihan()
    {
        return [
            "pokok" => $this->angsuran,
            "bunga" => $this->hitungBunga(),
            "denda" => 0,
        ];
    }

    public function bayar($pokok, $bunga, $denda)
    {
        $date = Carbon::now();

        $sisa = $this->sisa - $pokok;

        $jumlah = $pokok + $bunga + $denda;

        $pembayaran = $this->pembayaran()->create([
            'pokok' => $pokok, 
            'bunga' => $bunga, 
            'denda' => $denda, 
            'jumlah' => $jumlah, 
            'sisa' => $sisa, 
            'tanggal' => $date,
            'created_by' => Auth::id(),
        ]);

        $this->tanggal_pembayaran_selanjutnya = $date->addMonth(1)->format('Y-m-d');
        $this->sisa = $sisa;

        if ($sisa <= 0) {
            $this->lunas_at = $date;
        }

        $this->save();

        return $pembayaran;
    }
}