<?php

namespace App\Models\Pinjaman;

use Illuminate\Database\Eloquent\Model;

class Simulasi extends Model
{   
    protected $table = 'pinjaman_simulasi';

    protected $fillable = [ 
        'master_id', 'pokok', 'bunga', 'jumlah', 'sisa', 'tanggal',
    ];

    public function master()
    {
        return $this->belongsTo(Master::class, 'master_id');
    }
}