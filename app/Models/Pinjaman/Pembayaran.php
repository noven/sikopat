<?php

namespace App\Models\Pinjaman;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{   
    protected $table = 'pinjaman_pembayaran';

    protected $fillable = [ 
        'master_id', 'pokok', 'bunga', 'denda', 'jumlah', 'sisa', 'tanggal', 'created_by',
    ];

    public function master()
    {
        return $this->belongsTo(Master::class, 'master_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}