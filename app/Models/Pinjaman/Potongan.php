<?php

namespace App\Models\Pinjaman;

use Illuminate\Database\Eloquent\Model;

class Potongan extends Model
{   
    protected $table = 'pinjaman_potongan';

    protected $fillable = [ 
        'master_id', 'keterangan', 'nominal',
    ];

    public function master()
    {
        return $this->belongsTo(Master::class, 'master_id');
    }
}