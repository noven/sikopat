<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    const TOKEN_LIFETIME_KOLEKTOR = 480;

    protected $fillable = [
        'id', 'user_id', 'revoked', 'expires_at', 'device',
    ];

    protected $keyType = 'varchar';
    
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
#method

    /**
     * Cek apakah token sudah expired
     *
     * @return boolean
     */
    public function isExpired(): bool
    {
        $expiration = Carbon::parse($this->expires_at);
        
        return !$expiration->greaterThan(Carbon::now());
    }

    /**
     * Memperbarui masa berlaku token
     *
     * @return $this
     */
    public function renew()
    {   
        return $this->update(['expires_at' => Carbon::now()->addMinutes(self::TOKEN_LIFETIME_KOLEKTOR)]);
    }
#

}