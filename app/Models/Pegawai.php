<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $fillable = [
        'nama', 'telepon',
    ];

    public function user()
    {
        return $this->morphOne(User::class, 'userable');
    }

}