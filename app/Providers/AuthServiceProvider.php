<?php

namespace App\Providers;

use App\Models\Role;
use App\Models\User;
use App\Services\Auth\VenGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->extend('venguard', function ($app, $name, array $config) {
            $guard = new VenGuard($this->app['auth']->createUserProvider($config['provider']), $this->app['request']);
            $this->app->refresh('request', $guard, 'setRequest');

            return $guard;
        });

        if (Auth::user()) {
            $role = Auth::user()->role;
            $resources = $role->resources;

            $grouped = $resources->groupBy('param');

            if (!empty($grouped[0])) {
                foreach ($grouped[0] as $r) {
                    Gate::define($r->nama, function ($user) {
                        return true;
                    });
                }
            }

            // if (!empty($grouped[1])) {
            //     foreach ($grouped[1] as $r) {
            //         Gate::define($r->nama, function ($user) use ($r, $role) {
            //             switch ($r->nama) {
            //                 case 'tabungan_setor':
            //                     if ($role->id == Role::ROLE_KOLEKTOR) return true;
            //                     return Auth::opd()->can_mutasi;
            //                 case 'tabungan_tarik':
            //                     if ($role->id == Role::ROLE_KOLEKTOR) return true;
            //                     return Auth::opd()->can_mutasi;
            //                 default:
            //                     return true;
            //             }
            //         });
            //     }
            // }
        }
    }
}
