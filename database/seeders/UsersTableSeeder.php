<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tgl = date("Y-m-d H:i:s");

        DB::table('users')->insert([
            [
                'username' => 'admin',
                'password' => Hash::make('password'),
                'role_id' => Role::ROLE_ADMIN,
                'userable_type' => null,
                'userable_id' => null,
                'updated_at' => $tgl,
                'created_at' => $tgl,
            ],
            [
                'username' => 'putuwayan',
                'password' => Hash::make('password'),
                'role_id' => Role::ROLE_KOLEKTOR,
                'userable_type' => 'pegawai',
                'userable_id' => 1,
                'updated_at' => $tgl,
                'created_at' => $tgl,
            ],
        ]);
    }
}
