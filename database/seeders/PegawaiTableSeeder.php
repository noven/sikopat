<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PegawaiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tgl = date("Y-m-d H:i:s");

        DB::table('pegawai')->insert([
            [
                'nama' => 'Putu Wayan',
                'telepon' => '08123456789',
                'updated_at' => $tgl,
                'created_at' => $tgl,
            ],
        ]);
    }
}
