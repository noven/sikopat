<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tgl = date("Y-m-d H:i:s");

        DB::table('roles')->insert([
            [
                'id' => Role::ROLE_ADMIN,
                'nama' => 'ADMIN',
                'editable' => false,
                'updated_at' => $tgl,
                'created_at' => $tgl,
            ],
            [
                'id' => Role::ROLE_KOLEKTOR,
                'nama' => 'KOLEKTOR',
                'editable' => false,
                'updated_at' => $tgl,
                'created_at' => $tgl,
            ],
        ]);
    }
}
